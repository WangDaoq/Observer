package com.observer.Impl;

import com.observer.MyObserver;

public class Sister implements MyObserver{

	@Override
	public void up() {
	
		System.out.println("姐姐起来摸Baby");
	}

}
