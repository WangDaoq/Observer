package com.observer;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;



/**
 * ClassFinder
 * 
 * @author 
 * @date 2014-9-10
 * @version V1.0
 */
public class ClassFinder {

   
    private final static char DOT = '.';
    private final static char SLASH = '/';
    private final static String BAD_PACKAGE_ERROR = "Unable to get resources from path '%s'. Are you sure the given '%s' package exists?";

    /**
     * Method description
     * 
     * 
     * @param fileName
     * 
     * @return
     */
    public static String getClassNameFromFileName(String fileName) {
        String class_name = null;

        if (fileName.endsWith(".class")) {

            // class_name = fileName.substring(0,
            // fileName.length()-6).replace(File.separatorChar, '.');
            // Above code does not works on MS Windows if we load
            // files from jar file. Jar manipulation code always returns
            // file names with unix style separators
            String tmp_class_name = fileName.substring(0, fileName.length() - 6).replace('\\', '.');

            class_name = tmp_class_name.replace('/', '.');
        } // end of if (entry_name.endsWith(".class"))

        return class_name;
    }

    /**
     * Method description
     * 
     * 
     * @param jarFile
     * 
     * @return
     * 
     * @throws IOException
     */
    public static Set<String> getClassNamesFromJar(File jarFile) {
        Set<String> result = new TreeSet<String>();
        try {
            JarFile jar = new JarFile(jarFile);
            Enumeration<JarEntry> jar_entries = jar.entries();
            while (jar_entries.hasMoreElements()) {
                JarEntry jar_entry = jar_entries.nextElement();
                String class_name = getClassNameFromFileName(jar_entry.getName());

                if (class_name != null) {
                    result.add(class_name);
                    // System.out.println("class name: "+class_name);
                } // end of if (entry_name.endsWith(".class"))
            } // end of while (jar_entries.hasMoreElements())
        } catch (Exception e) {
            // TODO: handle exception
            //log.error(jarFile.getAbsolutePath(), e);
        }
        return result;
    }

    /*
     * 取得某一类所在包的所有类名 不含迭代
     */
    public static String[] getPackageAllClassName(String classLocation, String packageName) {
        // 将packageName分解
        String[] packagePathSplit = packageName.split("[.]");
        String realClassLocation = classLocation;
        int packageLength = packagePathSplit.length;
        for (int i = 0; i < packageLength; i++) {
            realClassLocation = realClassLocation + File.separator + packagePathSplit[i];
        }
        File packeageDir = new File(realClassLocation);
        if (packeageDir.isDirectory()) {
            String[] allClassName = packeageDir.list();
            return allClassName;
        }
        return null;
    }

    /**
     * 从包package中获取所有的Class
     * 
     * @param pack
     * @return
     */
    public static List<Class<?>> findImplementing(String packageName, final Class<?> implementedClazz) {

        // 第一个class类的集合
        List<Class<?>> classes = new ArrayList<Class<?>>();
        // 是否循环迭代
        boolean recursive = true;

        // 定义一个枚举的集合 并进行循环来处理这个目录下的things
        Enumeration<URL> dirs;
        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        final String packageDirName = packageName.replace(DOT, SLASH);
        try {
            dirs = classLoader.getResources(packageDirName);
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format(BAD_PACKAGE_ERROR, packageDirName, packageName), e);
        }

        try {
            // 循环迭代下去
            while (dirs.hasMoreElements()) {
                // 获取下一个元素
                URL url = dirs.nextElement();
                // 得到协议的名称
                String protocol = url.getProtocol();
                // 如果是以文件的形式保存在服务器上
                if ("file".equals(protocol)) {
                    // 获取包的物理路径
                    String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                    // 以文件的方式扫描整个包下的文件 并添加到集合中
                    findAndAddClassesInPackageByFile(packageName, filePath, recursive, classes, implementedClazz);
                } else if ("jar".equals(protocol)) {
                    // 如果是jar包文件
                    // 定义一个JarFile
                    JarFile jar;
                    try {
                        // 获取jar
                        jar = ((JarURLConnection) url.openConnection()).getJarFile();
                        // 从此jar包 得到一个枚举类
                        Enumeration<JarEntry> entries = jar.entries();
                        // 同样的进行循环迭代
                        while (entries.hasMoreElements()) {
                            // 获取jar里的一个实体 可以是目录 和一些jar包里的其他文件 如META-INF等文件
                            JarEntry entry = entries.nextElement();
                            String name = entry.getName();
                            // 如果是以/开头的
                            if (name.charAt(0) == '/') {
                                // 获取后面的字符串
                                name = name.substring(1);
                            }
                            // 如果前半部分和定义的包名相同
                            if (name.startsWith(packageDirName)) {
                                int idx = name.lastIndexOf('/');
                                // 如果以"/"结尾 是一个包
                                if (idx != -1) {
                                    // 获取包名 把"/"替换成"."
                                    packageName = name.substring(0, idx).replace('/', '.');
                                }
                                // 如果可以迭代下去 并且是一个包
                                if ((idx != -1) || recursive) {
                                    // 如果是一个.class文件 而且不是目录
                                    if (name.endsWith(".class") && !entry.isDirectory()) {
                                        // 去掉后面的".class" 获取真正的类名
                                        String className = name.substring(packageName.length() + 1, name.length() - 6);
                                        // 添加到集合中去
                                        className = packageName + '.' + className;
                                        addClassed(implementedClazz, classes, className);
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return classes;
    }

    /**
     * 以文件的形式来获取包下的所有Class
     * 
     * @param packageName
     * @param packagePath
     * @param recursive
     * @param classes
     */
    public static void findAndAddClassesInPackageByFile(String packageName, String packagePath,
            final boolean recursive, List<Class<?>> classes, final Class<?> implementedClazz) {
        // 获取此包的目录 建立一个File
        File dir = new File(packagePath);
        // 如果不存在或者 也不是目录就直接返回
        if (!dir.exists() || !dir.isDirectory()) {
            return;
        }
        // 如果存在 就获取包下的所有文件 包括目录
        File[] dirfiles = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String name) {
                return (recursive && file.isDirectory()) || (file.getName().endsWith(".class"));
            }
        });
        // 循环所有文件
        for (File file : dirfiles) {
            // 如果是目录 则继续扫描
            if (file.isDirectory()) {
                findAndAddClassesInPackageByFile(packageName + "." + file.getName(), file.getAbsolutePath(), recursive,
                        classes, implementedClazz);
            } else {
                // 如果是java类文件 去掉后面的.class 只留下类名
                String className = file.getName().substring(0, file.getName().length() - 6);
                // 添加到集合中去
                className = packageName + '.' + className;
                addClassed(implementedClazz, classes, className);
            }
        }
    }

    private static void addClassed(Class<?> implementedClazz, final List<Class<?>> classes, final String className) {
        try {
            Class<?> cls = Class.forName(className);
            if (implementedClazz == null) {
                classes.add(cls);
            } else {
                if (implementedClazz.isAssignableFrom(cls)) {
                    int mod = cls.getModifiers();
                    if (!Modifier.isAbstract(mod) && !Modifier.isInterface(mod) && Modifier.isPublic(mod)) {
                        classes.add(cls);
                    }
                }
            }
        } catch (ClassNotFoundException ignore) {
            //log.error("class not find", ignore);
        }
    }

    public static String getClassName(String name) {
    	 int idx = name.lastIndexOf('.');
    	 return name.substring(idx+1, name.length());
    }
    
    public static void main(String[] args) throws ClassNotFoundException {
    

        //Class<?> a = JSONObject.class;
        List<Class<?>> list = findImplementing("com.observer", MyObserver.class);
        //System.out.print(list.size());
        System.out.print(getClassName(list.get(0).toString()));
    }

}